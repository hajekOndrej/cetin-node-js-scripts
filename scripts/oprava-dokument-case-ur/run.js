const program = require("commander");
const worker = require("./src/worker");
const logger = require('./src/report');
const config = require("./config");
const common = require("./src/common")

const MODE = {
    validate: "validate",
    migrate: "migrate",
}

let mode = MODE["validate"]

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";

program
    .usage("")
    .option("-m, --mode <mode>", "specify in which mode program should run [validate,migrate] (default mode is validate)")

async function parseArgs() {

    program.parse(process.argv);

    //order-dependent parameters
    if (program.args[2]) {
        mode = MODE[program.args[2]]
        if (mode === undefined) {
            await logger.logGeneral(`Invalid mode: ${program.args[2]}. Choose from: [${MODE["validate"]}|${MODE["migrate"]}]`, false);
            process.exit(1);
        }
    }

    //parameters using option
    if (program.mode) {
        mode = MODE[program.mode]
        if (mode === undefined) {
            await logger.logGeneral(`Invalid mode: ${program.mode}. Choose from: [${MODE["validate"]}|${MODE["migrate"]}]`, false);
            process.exit(1);
        }
    }
}

/**
 * Main processing function.
 */
async function run() {

    await logger.setLogName(`${common.outLogName.LOG_NAME}.csv`)
    await logger.cleanStart();

    await parseArgs();

    await worker.work(mode);

}

run();













