## Configure the script
All configuration should be in config.js so configure your running environment there.
```
ID_SCRIPT_RUN_TITLE: "testRun1",                                               
PWF_BASE_URL: "https://pwf-1-dev.cetin",
ID_RESOURCE_SITARI_A_DOSS_CASE: "5f439443be03bc0010c7c9c4",
ID_RESOURCE_DOKUMENTY_CASE: "5f3e3a0a8171980010a28849",
JWT_TOKEN: "eyJhbGciOiJIUzUxMiJ9.eyJleHRlcm5hbCI6dHJ....  
```
## Running the script
In ../oprava-dokument-case-ur
```
1. npm install (only for the first time) => node_modules
2. npm run.js
```

### Available options
```
node run.js -h
->
 Options:
  -m, --mode <mode>          specify in which mode program should run [validate,migrate] (default mode is validate)
  -h, --help                 display help for command
```

### Validation example
- Only validation starts (validate is default mode)
- Informs about the number of submissions that will be updated
```
node run.js
```

### Migration example
- Validation starts and valid data are updated
```
node run.js -m migrate
```

## Output files
Log file will be created at the src folder level
### Log file
Record the progress of validation/migration.
All lines of the log are also printed to the console during the process.
```
../../logs/oprava-dokument-case-ur-testRun1-pwf-1-dev.cetin-220303_101728.csv
```

