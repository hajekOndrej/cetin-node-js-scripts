const fs = require('fs');
const createCsvWriterStream = require('csv-write-stream')
const common = require("./common")
const path = require('path')

// const logPath = '../../logs/'
// let logCSV
let logPath
let logCSV
const errorStorage = []

async function setLogName(confSpecLogName) {
    logPath = path.join(__dirname, '..','logs');
    logCSV = path.join(__dirname, '..','logs',confSpecLogName);
}

// Row data logging. isOk = true -> 'OK'(default) || isOk = false -> 'ERROR'
const logData = (CASE, data_id, description, isOk = true) => {
    return new Promise((resolve, reject) => {
        const logFn = isOk ? console.info : console.error
        const status = isOk ? 'OK' : 'ERROR';
        logFn(status + ':  ' + 'CASE: ' + CASE + '  data_id: ' + data_id + ' - ' + description)

        let csvWriterStream

        // If csv does not exist, it will create it. The next call is written to csv line by line
        if (!fs.existsSync(logCSV)) {
            csvWriterStream = createCsvWriterStream({
                separator: ';',
                headers: [
                    'timestamp',
                    'status',
                    'CASE',
                    'data_id',
                    'description'
                ],
            });
        } else {
            csvWriterStream = createCsvWriterStream({ sendHeaders: false, separator: ';' })
        }
        csvWriterStream.pipe(fs.createWriteStream(logCSV, { flags: 'a' }))
            .on("finish", resolve)
            .on("error", reject)
        csvWriterStream.write({
            timestamp: common.currentTime(),
            status: status,
            CASE: CASE,
            data_id: data_id,
            description: description
        });
        csvWriterStream.end()
    })
};

// General logging. isOk = true -> 'INFO'(default) || isOk = false -> 'ERROR'
const logGeneral = (description, isOk = true) => {
    return new Promise((resolve, reject) => {
        const logFn = isOk ? console.info : console.error
        const status = isOk ? 'INFO' : 'ERROR'
        logFn(status + ': ' + description)

        // Storing only Errors in memory (used for error check {function hasErrors()})
        if (!isOk) errorStorage.push({ status: status, description: description })

        let csvWriterStream

        if (!fs.existsSync(logCSV)) {
            csvWriterStream = createCsvWriterStream({
                separator: ';',
                headers: [
                    'timestamp',
                    'status',
                    'CASE',
                    'data_id',
                    'description'
                ],
            })
        } else {
            csvWriterStream = createCsvWriterStream({ sendHeaders: false, separator: ';' })
        }
        csvWriterStream.pipe(fs.createWriteStream(logCSV, { flags: 'a' }))
            .on("finish", resolve)
            .on("error", reject)
        csvWriterStream.write({
            timestamp: common.currentTime(),
            status: status,
            CASE: '',
            data_id: '',
            description: description
        });
        csvWriterStream.end()
    })
};

// Error check - returns true when an error was written
function hasErrors() {
    return !!errorStorage.find(item => item.status === 'ERROR');
}

// deletes-creates/create a new csv with a header
async function cleanStart() {
    if (fs.existsSync(logPath)) {
        try {
            await logGeneral("New Log file created : " + logCSV)
        } catch (err) {
            await console.log("Cannot create log file : " + err);
            process.exit(1);
        }
    } else {
        try {
            await fs.mkdirSync(logPath)
            await logGeneral("New logs folder created : " + logPath)
            await logGeneral("New Log file created : " + logCSV)
        } catch (err) {
            await console.log("Cannot create logs folder : " + err);
            process.exit(1);
        }
    }
}

module.exports = {
    logData,
    logGeneral,
    hasErrors,
    cleanStart,
    setLogName
};
