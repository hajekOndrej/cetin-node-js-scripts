const logger = require("./report");
const formio = require("./formio");
const config = require("../config");
const common = require("./common");

// Dohledání všech submission případu “Síťaři a DoSS - CASE”
async function sitariADossSubmissionsAsList() {
    const response = await formio.getResourceSubmissionsWithoutValidationForm(config.ID_RESOURCE_SITARI_A_DOSS_CASE,
        `limit=999999&select=_id`);
    let sitariADossCaseList = []
    if (!common.wasResourceFound(response)) {
        logger.logData('Síťaři a DoSS - CASE', '#', `Bylo nalezeno ${response.length} záznamů. Nebude proveden žádná aktualizace případů Dokumenty - CASE.`)
        return sitariADossCaseList
    } else {
        logger.logData('Síťaři a DoSS - CASE', '#', `Bylo nalezeno ${response.length} záznamů.`)
        for await (const element of response){
               sitariADossCaseList.push(element._id)
               }
        return sitariADossCaseList
    }
}

// Dohledat všechny submission případu “Dokumenty - CASE” které v atributu “ur” odkazují na nějakou submission
async function dokumentySubmissionsAsList() {
    const response = await formio.getResourceSubmissionsWithoutValidationForm(config.ID_RESOURCE_DOKUMENTY_CASE,
        `limit=999999&data.ur._id__exists='true'&select=_id`);
    let allDokumentyCaseList = []
    if (!common.wasResourceFound(response)) {
        logger.logData('Dokumenty - CASE', '#', `Bylo nalezeno ${response.length} záznamů. Nebude proveden žádná aktualizace případů Dokumenty - CASE.`)
        return allDokumentyCaseList
    } else {
        logger.logData('Dokumenty - CASE', '#', `Bylo nalezeno ${response.length} záznamů s vazbou ur.`)
        for await (const element of response){
                allDokumentyCaseList.push(element._id)
                }
        return allDokumentyCaseList
    }
}

// Vyfiltrovat všechny submission případu “Dokumenty - CASE” které v atributu “ur” odkazují na nějakou submission případu “Síťaři a DoSS - CASE”
async function dokumentySubmissionsUrOnlyAsList(sitariADossCaseList,allDokumentyCaseList) {

    let dokumentyCaseList = []
    let counter = 0;
    for await (const element of allDokumentyCaseList){

        const response = await formio.getResourceSubmission(config.ID_RESOURCE_DOKUMENTY_CASE,element);
        const ur = response.data.ur._id
        if(sitariADossCaseList.includes(ur)){
            dokumentyCaseList.push({_id:element,ur:ur})
            console.log(`Dokument ${element} je špatně.`)
        }
        console.log(`Dokument no. ${counter}/${allDokumentyCaseList.length}`)
        counter++;
    }

    if(dokumentyCaseList.length === 0){
    logger.logData('Dokumenty - CASE', '#', `Bylo nalezeno ${dokumentyCaseList.length} záznamů s chybnou vazbou ur na Síťaři a DoSS - CASE. Nebude proveden žádná aktualizace případů Dokumenty - CASE.`)
    }else{
    logger.logData('Dokumenty - CASE', '#', `Bylo nalezeno ${dokumentyCaseList.length} záznamů s chybnou vazbou ur na Síťaři a DoSS - CASE.`)
    }

    return dokumentyCaseList
}

//-------------------- Validace na "Bad Token" --------------------
async function checkValidJwtToken() {
    let resourceResponse;
    let isValid = true;

    try {
        resourceResponse = await formio.getResource(config.ID_RESOURCE_SUBPROJEKT_CASE);
    } catch (err) {
        console.error(err);
        throw Error("Connection error: " + err.message);
    }

    if (resourceResponse == "Bad Token") {
        isValid = false;
    }
    return isValid;
}

module.exports = {
    sitariADossSubmissionsAsList,
    dokumentySubmissionsAsList,
    dokumentySubmissionsUrOnlyAsList,
    checkValidJwtToken
}
