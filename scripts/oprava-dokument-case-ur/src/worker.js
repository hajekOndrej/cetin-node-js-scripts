const logger = require('./report');
const getData = require("./getData")
const setData = require("./setData")

async function work(mode) {
console.log(mode)
    if (mode === "migrate") {
        logger.logGeneral("- Submissions updating started..")
    } else {
        logger.logGeneral("- Submissions search to update started ..")
    }

    // Check valid JWT token
    logger.logGeneral("- Check valid JWT token")
    if (!await getData.checkValidJwtToken()) {

        console.error("JWT Token in the configuration is invalid.");
        throw Error("JWT Token in the configuration is invalid.");
    }

    // Dohledání všech submission případu “Síťaři a DoSS - CASE”
    let sitariADossCaseList = await getData.sitariADossSubmissionsAsList()
//    console.log(sitariADossCaseList)

    // Dohledat všechny submission případu “Dokumenty - CASE” které v atributu “ur” odkazují na nějakou submission
    let allDokumentyCaseList = await getData.dokumentySubmissionsAsList()
//    console.log(allDokumentyCaseList)

    // Vyfiltrovat všechny submission případu “Dokumenty - CASE” které v atributu “ur” odkazují na nějakou submission případu “Síťaři a DoSS - CASE”
    let dokumentyCaseList = await getData.dokumentySubmissionsUrOnlyAsList(sitariADossCaseList,allDokumentyCaseList)
//    console.log(dokumentyCaseList)

    if(mode === "migrate"){
        // Aktualizace vazeb Dokumenty - CASE na Síťaři a DoSS - CASE
        await setData.updateDokumentyLinksToSitariADoss(dokumentyCaseList)
    }
    if (mode === "migrate") {
        logger.logGeneral("- Migration of individual rows finished")
    } else {
        logger.logGeneral("- Validation of individual rows finished")
    }
}

module.exports = {
    work
};
