"use strict";
const fs = require("fs");
//const moment = require("moment")
//const { getJsDateFromExcel } = require("excel-date-to-js");
const { format, isMatch } = require("date-fns");
//const service = require("./pwfservice");
const { ID_SCRIPT_RUN_TITLE, PWF_BASE_URL } = require("../config");

function wasFound(response) {
    return response && !("error" in response);
}

function wasResourceFound(response) {
    return (
        !!response &&
        typeof response !== "string" &&
        (!Array.isArray(response) || response.length > 0)
    );
}

//async function base64ToXlsx(base64, outXLSX) {
//    let data = await fs.readFileSync(base64, "utf8");
//    let buff = await new Buffer.from(data, "base64");
//    await fs.writeFileSync(outXLSX, buff);
//}

// Returns the current time
function currentTime() {
    const today = new Date();
    const date =
        ("0" + today.getDate()).slice(-2) +
        "." +
        ("0" + (today.getMonth() + 1)).slice(-2) +
        "." +
        today.getFullYear();
    const time =
        ("0" + today.getHours()).slice(-2) +
        ":" +
        ("0" + today.getMinutes()).slice(-2) +
        ":" +
        ("0" + today.getSeconds()).slice(-2);
    return date + " " + time;
}

const formatDateForLog = () => {
    return format(new Date(), "yyMMdd_HHmmss");
};

const outLogName = {
    LOG_NAME: `oprava-dokument-case-ur-${ID_SCRIPT_RUN_TITLE}-${PWF_BASE_URL.slice(8)}-${formatDateForLog()}`
};

//const isNaturalNumber = (n) => {
//    n = n.toString();
//    const n1 = Math.abs(n),
//        n2 = parseInt(n, 10);
//    return !isNaN(n1) && n2 === n1 && n1.toString() === n;
//};
//const isRationalNumber = (n) => {
//    if (typeof n === "number") {
//        return n - Math.floor(n) !== 0;
//    } else {
//        return false;
//    }
//};

//const isDateFormatValid = (value) => {
//    if (typeof value === "number" && value !== 0) {
//        // Parse date from excel format and cut the last character in date string ("..Z") to adjust UTC formating
//        const parsedExcelDate = new Date(
//            getJsDateFromExcel(value).toISOString().slice(0, -1)
//        );
//        // Required pattern is the second parameter of format function
//        const formatedDate = format(parsedExcelDate, "dd.MM.yyyy HH:mm:ss");
//        return isMatch(formatedDate, "dd.MM.yyyy HH:mm:ss");
//    }
//};

//const cellIsEmpty = (value) =>
//    (typeof value === "string" && value === "") || typeof value === "undefined";

//const returnIfColumnIsOptional = (value) => {
//    if (cellIsEmpty(value)) {
//        return {
//            isValid: true,
//            newValue: "",
//        };
//    } else {
//        return {
//            isValid: true,
//            newValue: value,
//        };
//    }
//};

//const returnIfIsAnyOptionalNumber = (value) => {
//    if (cellIsEmpty(value)) {
//        return {
//            isValid: true,
//            newValue: "",
//        };
//    } else if (typeof value === "number") {
//        return {
//            isValid: true,
//            newValue: value,
//        };
//    } else {
//        return {
//            isValid: false,
//        };
//    }
//};

//const returnIfIsDate = (value) => {
//    if (cellIsEmpty(value)) {
//        return {
//            isValid: true,
//            newValue: "",
//        };
//    } else if (isDateFormatValid(value)) { // excel date as double (number) value
//        return {
//            isValid: true,
//            newValue: convertExcelDateToLocalZoneDate(getJsDateFromExcel(value)),
//        };
//    } else if (typeof value === "string" && value.length > 0) { // excel date as text (string) value
//        const dateFromText = moment(value, "DD.MM.YYYY HH:mm:ss", true);
//        if (!dateFromText.isValid()) {
//            return {
//                isValid: false,
//            };
//        }
//        return {
//            isValid: true,
//            newValue: dateFromText.toDate(),
//        };
//    } else {
//        return {
//            isValid: false,
//        };
//    }
//};

//const convertExcelDateToLocalZoneDate = (/*Date*/excelDate) => {
//    if (!excelDate) return null;
//    let mom = moment(`${excelDate.getUTCFullYear()}-${excelDate.getUTCMonth() + 1}-${excelDate.getUTCDate()}
//                T${excelDate.getUTCHours()}:${excelDate.getUTCMinutes()}:${excelDate.getUTCSeconds()}`,
//        "YYYY-MM-DDTHH:mm:ss");
//    return mom.toDate();
//};

async function checkValidJwtToken(jwtToken) {
    await service
        .validUserByToken(jwtToken)
        .then((res) => {
            if (res === "Unauthorized" || res === "Bad Token") {
                throw Error(`JWT Token není platný: ${jwtToken}`);
            }
        })
        .catch((err) => {
            throw Error(err.message);
        });
}

/**
 * e.g. "NejkulaťOULinkatějšÍ" => "nejkulatoulinkatejsi"
 */
const normalizeString = str => {
    return str.normalize("NFD").replace(/\p{Diacritic}/gu, "").toLowerCase();
};

const chunkArray = (array, chunkSize) => Array(Math.ceil(array.length / chunkSize))
    .fill(undefined)
    .map((_, index) => index * chunkSize)
    .map(begin => array.slice(begin, begin + chunkSize));

/**
 * e.g. { foo: " yo", bar: "hello", baz: "   hi " } => { foo: "yo", bar: "hello", baz: "hi" }
 */
const trimStringsInObject = obj => {
    Object.keys(obj).map(key => obj[key] = typeof obj[key] === "string" ? obj[key].trim() : obj[key]);
};

/**
 * e.g. { foo: "", bar: "something", baz: null } => { bar: "something" }
 */
const cleanEmptyStringsInObject = obj => {
    let propNames = Object.getOwnPropertyNames(obj);
    for (let i = 0; i < propNames.length; i++) {
        let propName = propNames[i];
        if (obj[propName] === null || obj[propName] === undefined || (typeof obj[propName] === "string" && obj[propName].length <= 0)) {
            delete obj[propName];
        }
    }
};

module.exports = {
    wasFound,
    wasResourceFound,
//    base64ToXlsx,
    currentTime,
    outLogName,
//    isNaturalNumber,
//    isRationalNumber,
//    isDateFormatValid,
//    cellIsEmpty,
//    returnIfColumnIsOptional,
//    returnIfIsAnyOptionalNumber,
//    returnIfIsDate,
//    checkValidJwtToken,
//    normalizeString,
//    chunkArray,
//    trimStringsInObject,
//    cleanEmptyStringsInObject,
};
