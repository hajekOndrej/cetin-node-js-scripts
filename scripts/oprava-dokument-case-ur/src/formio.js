"use strict";
const request = require("request");
const {PWF_BASE_URL, JWT_TOKEN} = require("../config");

async function getResource(resourceId) {
    return new Promise(function (resolve, reject) {
        request.get({
            url: `${PWF_BASE_URL}/form/form/${resourceId}`,
            json: true,
            headers: {"x-jwt-token": JWT_TOKEN},
        }, (err, res, data) => {
            if (err) {
                reject(err);
            }
            // data is already parsed as JSON:
            resolve(data);
        });
    });
}

async function getResourceSubmission(resourceId, submissionId) {
    return new Promise(function (resolve, reject) {
        request.get({
            url: `${PWF_BASE_URL}/form/form/${resourceId}/submission/${submissionId}?select=data.ur._id`,
            json: true,
            headers: {"x-jwt-token": JWT_TOKEN},
        }, (err, res, data) => {
            if (err) {
                reject(err);
            }
            if (!!res?.statusCode && `${res?.statusCode}`.charAt(0) === "4") {
                reject(res.statusMessage)
            }
            // data is already parsed as JSON:
            resolve(data);
        });
    });
}

async function getResourceSubmissions(resourceId, filter) {
    return new Promise(function (resolve, reject) {
        let url = encodeURI(`${PWF_BASE_URL}/form/form/${resourceId}/submission?${filter || ""}&validationForm=${resourceId}`)
        request.get({
            url: url,
            json: true,
            headers: {"x-jwt-token": JWT_TOKEN},
        }, (err, res, data) => {
            if (err) {
                reject(err);
            }
            // data is already parsed as JSON:
            resolve(data);
        });
    });
}

async function getResourceSubmissionsWithoutValidationForm(resourceId, filter) {
    return new Promise(function (resolve, reject) {
        request.get({
            url: `${PWF_BASE_URL}/form/form/${resourceId}/submission?${filter || ""}`,
            json: true,
            headers: {"x-jwt-token": JWT_TOKEN},
        }, (err, res, data) => {
            if (err) {
                reject(err);
            }
            // data is already parsed as JSON:
            resolve(data);
        });
    });
}

async function postNewSubmission(resourceId, data) {
    return new Promise(function (resolve, reject) {
        request.post({
            url: `${PWF_BASE_URL}/form/form/${resourceId}/submission`,
            json: {
                data,
            },
            headers: {"x-jwt-token": JWT_TOKEN},
        }, (err, res, data) => {
            if (err) {
                reject(err);
            }
            // data is already parsed as JSON:
            resolve(data);
        });
    });
}

async function patchExistingSubmission(resourceId, submissionId, patchData) {
    return new Promise(function (resolve, reject) {
        request.patch({
            url: `${PWF_BASE_URL}/form/form/${resourceId}/submission/${submissionId}`,
            json: patchData,
            headers: {"x-jwt-token": JWT_TOKEN},
        }, (err, res, data) => {
            if (err) {
                reject(err);
            }
            if (!!res?.statusCode && `${res?.statusCode}`.charAt(0) === "4") {
                reject(res.statusMessage)
            }
            // data is already parsed as JSON:
            resolve(data);
        });
    });
}

async function delResourceSubmission(resourceId, submissionId) {
    return new Promise(function (resolve, reject) {
        request.del({
            url: `${PWF_BASE_URL}/form/form/${resourceId}/submission/${submissionId}`,
            json: true,
            headers: {"x-jwt-token": JWT_TOKEN},
        }, (err, res, data) => {
            if (err) {
                reject(err);
            }
            // data is already parsed as JSON:
            resolve(data);
        });
    });
}

module.exports = {
    getResource,
    getResourceSubmission,
    getResourceSubmissions,
    getResourceSubmissionsWithoutValidationForm,
    postNewSubmission,
    patchExistingSubmission,
    delResourceSubmission
};
