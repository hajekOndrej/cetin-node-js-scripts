const logger = require("./report");
const formio = require("./formio");
const config = require("../config");
const common = require("./common");

// Aktualizace vazeb Dokumenty - CASE na Síťaři a DoSS - CASE
async function updateDokumentyLinksToSitariADoss(dokumentyCaseList){

//    await dokumentyCaseList.forEach(element => {
    for (const element of dokumentyCaseList){
        let patchData = [
            {
                op: "add",
                path: "/data/urSitariAaDoss",
                value: {
                    _id: element.ur,
                }
            },
            {
                op: "remove",
                path: "/data/ur"
            }
        ];
        try {
            const updateResponse = await formio.patchExistingSubmission(config.ID_RESOURCE_DOKUMENTY_CASE, element._id, patchData);
            if(!common.wasResourceFound(updateResponse) || updateResponse._id === undefined){
                logger.logData('Dokumenty - CASE', element._id, `Nepodařilo se navázat Síťaři a DoSS - CASE _id: ${element.ur} na Dokumenty - CASE _id: ${element._id}.API POST call response: ${updateResponse}` , false)
            }else{
                logger.logData('Dokumenty - CASE',element._id, `Vazba na DoSS - CASE _id: ${element.ur} úspěšně navázán přes "urSitariAaDoss". Vazba přes "ur" odmazána` , true)
            }
        }catch (err){
                logger.logData('Dokumenty - CASE',element._id,`Nepodařilo se upravit vazbu na DoSS - CASE _id: ${element.ur}. Message : ` + err,false)
            }
    }
}

module.exports = {
    updateDokumentyLinksToSitariADoss
};































